package middleware

import (
	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"github.com/imroc/req"
	"net/http"
)


type Resource struct {
	Err 		string	`json:"error"`
	ErrDesc		string	`json:"error_description"`
	AccToken	string	`json:"access_token"`
	ClientId	string	`json:"client_id"`
	UserId		string	`json:"user_id"`
	Expires		string	`json:"expires"`
}



type Message struct {
	Msg		string		`json:"message"`
}


const ClientId = "Tc9kIscpUjf5BlL9NJpgHdRfWXrwyenV"

func SSO(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		authorization := c.Request().Header.Get("Authorization")
		
		header := req.Header{
			"Accept" : "application/json",
			"Authorization": authorization,
		}
	
		r, err := req.Get("http://oauth.infralabs.cs.ui.ac.id/oauth/resource", header)
		if err != nil {
			logrus.Println(err)
			return
		}
		
		resource := &Resource{}
		r.ToJSON(resource)

		if resource.UserId != ""  && resource.ClientId == ClientId {
			return next(c)
		}  



		err = c.JSON(http.StatusUnauthorized, &Message{Msg: `Not Authorized, get your token at http://oauth.infralabs.cs.ui.ac.id/oauth/token`})
		return 
	}
}