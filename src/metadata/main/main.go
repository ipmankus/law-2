package main

import (

	"metadata/models"
	_metadataUsecase "metadata/usecase"
	_metadataHttpDelivery "metadata/delivery/http"
	"middleware"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	
)

func init() {
	viper.SetConfigFile(`config.json`)
	
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func main() {

	e := echo.New()
	e.Use(middleware.SSO)
	db, err := gorm.Open("sqlite3", viper.GetString("database.sqlite3_loc"))
	if err != nil {
		logrus.Fatal(err)
	}
	defer db.Close()
	db.AutoMigrate(&models.Metadata{})

	
	metadataUsecase := _metadataUsecase.NewMetadataUsecase(db)
	_metadataHttpDelivery.NewMetadataHandler(e, metadataUsecase)
	


	serverPort := viper.GetString("server.address_metadata")
	logrus.Fatal(e.Start(serverPort))
}
