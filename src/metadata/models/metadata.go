package models

import (
	"time"
)

type Metadata struct {
	ID        	uint 		`gorm:"primary_key"`
	CreatedAt 	time.Time
	UpdatedAt 	time.Time
	DeletedAt 	*time.Time
	FileName	string		`gorm:"size:64"`
	FileLoc		string		`gorm:"size:64"`
}
