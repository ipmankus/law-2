package http


import (
	"net/http"
	"context"
	"fmt"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"metadata"
	"metadata/models"
	"utils"
)

type MetadataHandler struct {
	MUsecase metadata.Usecase
}


func NewMetadataHandler(e *echo.Echo, us metadata.Usecase){
	handler := &MetadataHandler{
		MUsecase: us,
	}
	e.POST("/create", handler.AddMetadata)
	e.GET("/get/:id", handler.GetMetadata)
	e.POST("/delete", handler.DeleteMetadata)
	e.POST("/update", handler.UpdateMetadata)
}


type CreateRequest struct {
  FileLoc  string `json:"location"`
  FileName string `json:"filename"`
}

type IDRequest struct {
	ID  uint `json:"id"`	
}

type IDCRequest struct {
	ID uint `json:"id"`
	Content CreateRequest `json:"content"`
}

type Message struct {
	Msg  string `json:"message"`
}


func (a *MetadataHandler) AddMetadata(c echo.Context) (err error) {
	rq := &CreateRequest{}
	if err = c.Bind(rq); err != nil {
		return utils.ErrJSONMetadata
	}
	
	err = a.MUsecase.Create(context.Background(), &models.Metadata{FileLoc: rq.FileLoc, FileName: rq.FileName})
	if err != nil {
		return 
	}
	mt := &models.Metadata{}
	err = a.MUsecase.ReadLast(context.Background(), mt)
	if err != nil {
		logrus.Fatal(err)
	}
	err = c.JSON(http.StatusOK, &Message{Msg: fmt.Sprintf("Created at /get/%d", mt.ID)})
	return
}

func (a *MetadataHandler) GetMetadata(c echo.Context) (err error) {
	var id uint
 	fmt.Sscanf(c.Param("id"), "%d", &id)

	mt := &models.Metadata{}
	err = a.MUsecase.Read(context.Background(), id, mt)
	if err != nil {
		return
	}
	err = c.JSON(http.StatusOK, mt)
	return
}


func (a *MetadataHandler) DeleteMetadata(c echo.Context) (err error) {
	dq := &IDRequest{}

	if err = c.Bind(dq); err != nil {
		return utils.ErrJSONMetadataID
	}
	err = a.MUsecase.Delete(context.Background(), dq.ID)
	if err != nil {
		return
	}
	err = c.JSON(http.StatusOK, &Message{Msg: fmt.Sprintf("Deleted %d", dq.ID)})
	return
}

func (a *MetadataHandler) UpdateMetadata(c echo.Context) (err error) {
	// Update(c context.Context, id uint, mt *models.Metadata)
	dq := &IDCRequest{}

	if err = c.Bind(dq); err != nil {
		return utils.ErrJSONMetadataIDC
	}
	err = a.MUsecase.Update(context.Background(), dq.ID, &models.Metadata{FileLoc: dq.Content.FileLoc, FileName: dq.Content.FileName})
	if err != nil {
		return
	}
	err = c.JSON(http.StatusOK, &Message{Msg: fmt.Sprintf("Updated at /get/%d", dq.ID)})
	return
}

