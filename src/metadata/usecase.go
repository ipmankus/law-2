package metadata

import (
	"context"
	"metadata/models"
)

type Usecase interface {
	Create(context.Context, *models.Metadata) (error)
	Read(context.Context, uint, *models.Metadata) (error)
	ReadLast(context.Context, *models.Metadata) (error)
	Update(context.Context, uint, *models.Metadata) (error)
	Delete(context.Context, uint) (error) 
}
