package usecase

import (
	"context"
	"github.com/jinzhu/gorm"
	"metadata/models"
	"metadata"
)

type MetadataUsecase struct {
	db *gorm.DB
}

func NewMetadataUsecase(db *gorm.DB) metadata.Usecase {
	return &MetadataUsecase{
		db,
	}
}

func (a *MetadataUsecase) Create(c context.Context, mt *models.Metadata) (err error){
	err = a.db.Create(mt).Error
	return
}

func (a *MetadataUsecase) Delete(c context.Context, id uint) (err error) {
	err = a.db.Delete(&models.Metadata{ID: id}).Error
	return
}

func (a *MetadataUsecase) Read(c context.Context, id uint, mt *models.Metadata) (err error) {
	err = a.db.First(mt, id).Error
	return
}

func (a *MetadataUsecase) ReadLast(c context.Context, mt *models.Metadata) (err error) {
	err = a.db.Last(mt).Error
	return
}

func (a *MetadataUsecase) Update(c context.Context, id uint, mt *models.Metadata) (err error) {
	metadata := &models.Metadata{ID: id}
	err = a.db.Model(metadata).Updates(mt).Error
	return
}



