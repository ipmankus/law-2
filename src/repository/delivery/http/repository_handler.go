package http


import (
	"net/http"
	"io/ioutil"
	"context"
	"fmt"

	"github.com/labstack/echo"
	// "github.com/sirupsen/logrus"
	"repository"
	"utils"
)

type RepositoryHandler struct {
	RUsecase repository.Usecase
}

type UploadMsg struct {
	Message string `json:"message"`
}


func NewRepositoryHandler(e *echo.Echo, us repository.Usecase){
	handler := &RepositoryHandler{
		RUsecase: us,
	}
	e.POST("/upload", handler.Upload)
	e.GET("/uploaded/:filename", handler.Download)
}

func (a *RepositoryHandler) Upload(c echo.Context) (err error) {
	
	defer func(){
		caught := recover()
		if caught != nil {
			err = utils.ErrFileNil
			return 
		}
	}()
	
	file, err := c.FormFile("file")
	if err!=nil{
		return
	}
	
	src, err := file.Open()
	if err!=nil{
		return
	}
	defer src.Close()
	
	bytes, err := ioutil.ReadAll(src)
	if err!=nil{
		return
	}

	var filename string
	filename, err = a.RUsecase.SaveFile(context.Background(), bytes)
	
	if err != nil {
		return
	}
	
	err = c.JSON(http.StatusOK, &UploadMsg{Message: fmt.Sprintf("File successfully uploaded /uploaded/%s", filename)})
	return
}


func (a *RepositoryHandler) Download(c echo.Context) (err error) {
	filename := c.Param("filename")

	bytes, err := a.RUsecase.FetchFile(context.Background(), filename)
	
	if err != nil {
		if err == utils.IErrHacker {
			err = utils.ErrHacker
		} else {
			err = utils.ErrFileNotFound
		}
		return
	}
	
	c.Response().Header().Set(echo.HeaderContentDisposition, "attachment;")
	err = c.String(http.StatusOK, string(bytes))
	return
}