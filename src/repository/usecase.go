package repository

import (
	"context"
)

type Usecase interface {
	SaveFile(context.Context, []byte) (string, error)
	FetchFile(context.Context, string) ([]byte, error)
}
