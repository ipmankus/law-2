package main

import (
	_repositoryUsecase "repository/usecase"
	_repositoryHttpDelivery "repository/delivery/http"
	"middleware"
	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"math/rand"
	"time"
)

func init() {
	viper.SetConfigFile(`config.json`)
	rand.Seed(time.Now().UnixNano())
	err := viper.ReadInConfig()

	if err != nil {
		panic(err)
	}
}

func main() {

	e := echo.New()
	e.Use(middleware.SSO)
	repositoryUsecase := _repositoryUsecase.NewRepositoryUsecase(viper.GetString("repository.location"))
	_repositoryHttpDelivery.NewRepositoryHandler(e, repositoryUsecase)

	serverPort := viper.GetString("server.address_repository")
	logrus.Fatal(e.Start(serverPort))
}
