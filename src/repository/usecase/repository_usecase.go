package usecase

import (
	"io/ioutil"
	"context"
	"math/rand"
	"path/filepath"
	"os"
	"strings"
	
	"github.com/sirupsen/logrus"
	"repository"
	"utils"
)



type RepositoryUsecase struct {
	Location string
}

func NewRepositoryUsecase(Location string) repository.Usecase {
	err := os.MkdirAll(Location, 0644)
	if err != nil {
		logrus.Fatal(err)
	}
	return &RepositoryUsecase{
		Location,
	}
}


func (a *RepositoryUsecase) SaveFile(c context.Context, data []byte) (filename string, err error){
	filename = randStringRunes(16)
	err = ioutil.WriteFile(filepath.Join(a.Location, filename), data, 0644)
	return 
}


func (a *RepositoryUsecase) FetchFile(c context.Context, filename string) (data []byte, err error){
	if strings.ContainsAny(filename, `./\`){
		err = utils.IErrHacker
		return
	}
	data, err = ioutil.ReadFile(filepath.Join(a.Location, filename)) 
	return
}


var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randStringRunes(n int) string {
    b := make([]rune, n)
    for i := range b {
        b[i] = letterRunes[rand.Intn(len(letterRunes))]
    }
    return string(b)
}