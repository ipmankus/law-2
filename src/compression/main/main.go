package main

import (
	_compressionUsecase "compression/usecase"
	_compressionHttpDelivery "compression/delivery/http"
	"middleware"
	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	
	"time"
)

func init() {
	viper.SetConfigFile(`config.json`)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func main() {

	e := echo.New()
	e.Use(middleware.SSO)
	compressionUsecase := _compressionUsecase.NewCompressionUsecase(time.Duration(500) * time.Millisecond)
	_compressionHttpDelivery.NewCompressionHandler(e, compressionUsecase)


	// logrus.Println(compressionUsecase.CompressWithGzip(context.Background(), []byte(`hello`)))

	serverPort := viper.GetString("server.address_compression")
	logrus.Fatal(e.Start(serverPort))
}
