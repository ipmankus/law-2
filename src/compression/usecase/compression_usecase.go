package usecase

import (
	"bytes"
	"compress/gzip"
	"compression"
	"context"
	"errors"
	"time"
)

type CompressionUsecase struct {
	contextTimeout time.Duration
}

func NewCompressionUsecase(timeout time.Duration) compression.Usecase {
	return &CompressionUsecase{
		contextTimeout: timeout,
	}
}

func (a *CompressionUsecase) CompressWithGzip(c context.Context, data []byte) (result []byte, err error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	compressResult := make(chan []byte)
	compressErr := make(chan error, 1)
	go gzipData(ctx, data, compressResult, compressErr)

	select {
	case <-ctx.Done():
		err = errors.New(`Timelimit exceeded`)
		cancel()
		return
	case result = <-compressResult:
		err = <-compressErr
		return
	}

}

func gzipData(c context.Context, data []byte, compressResult chan []byte, compressErr chan error) {

	var b bytes.Buffer
	var result []byte
	var err error

	gz := gzip.NewWriter(&b)

	if _, err = gz.Write(data); err != nil {
		defer func() { compressErr <- err; compressResult <- result }()
		return
	}

	if err = gz.Flush(); err != nil {
		defer func() { compressErr <- err; compressResult <- result }()
		return
	}

	if err = gz.Close(); err != nil {
		defer func() { compressErr <- err; compressResult <- result }()
		return
	}

	result = b.Bytes()

	defer func() { compressErr <- err; compressResult <- result }()
	return
}
