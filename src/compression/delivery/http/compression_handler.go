package http


import (
	"net/http"
	"io/ioutil"
	"context"

	"github.com/labstack/echo"
	// "github.com/sirupsen/logrus"
	"compression"
	"utils"
)

type CompressionHandler struct {
	CUsecase compression.Usecase
}


func NewCompressionHandler(e *echo.Echo, us compression.Usecase){
	handler := &CompressionHandler{
		CUsecase: us,
	}
	e.POST("/compress", handler.Compress)
}

func (ch *CompressionHandler) Compress(c echo.Context) (err error) {
	
	defer func(){
		caught := recover()
		if caught != nil {
			err = utils.ErrFileNil
			return 
		}
	}()
	
	file, err := c.FormFile("file")
	if err!=nil{
		return
	}
	
	src, err := file.Open()
	if err!=nil{
		return
	}
	defer src.Close()
	
	bytes, err := ioutil.ReadAll(src)
	if err!=nil{
		return
	}

	var zippedContent string
	bytes, err = ch.CUsecase.CompressWithGzip(context.Background(), bytes)
	zippedContent = string(bytes)

	if err != nil {
		return
	}
	
	c.Response().Header().Set(echo.HeaderContentDisposition, "attachment;")
	c.Response().Header().Set(echo.HeaderContentType, "application/gzip")
	err = c.String(http.StatusOK, zippedContent)
	return
}