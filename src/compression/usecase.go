package compression

import (
	"context"
)

type Usecase interface {
	CompressWithGzip(context.Context, []byte) ([]byte, error)
}
