package utils

import (
	"github.com/labstack/echo"
	"net/http"
	"errors"
)

var (
	ErrFileNil       	= echo.NewHTTPError(http.StatusBadRequest, `Bad Request: Please upload a file`)
	ErrFileNotFound  	= echo.NewHTTPError(http.StatusNotFound, `Not Found: File not found`)
	ErrJSONMetadata  	= echo.NewHTTPError(http.StatusBadRequest, `Please supply correct json with type {"filename":<str>, "location": <str>}`)
	ErrJSONMetadataID   = echo.NewHTTPError(http.StatusBadRequest, `Please supply correct json with type {"id": <int>}`)
	ErrJSONMetadataIDC  = echo.NewHTTPError(http.StatusBadRequest, `Please supply correct json with type {"id": <int>, "content": {"filename":<str>, "location": <str>}}`)
	IErrHacker 		 	= errors.New(`hacker jangan macam-macam`)
	ErrHacker 		 	= echo.NewHTTPError(http.StatusBadRequest, `Bad Request: hacker jangan macam-macam`)
)
