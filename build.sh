export GOPATH=$(pwd)
go get github.com/lib/pq
go get github.com/sirupsen/logrus
go get github.com/labstack/echo
go get github.com/spf13/viper
go get github.com/jinzhu/gorm
go get github.com/jinzhu/gorm/dialects/sqlite
go get github.com/imroc/req
go build $1